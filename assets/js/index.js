$('document').ready(function(){
    setDepenses();
    document.getElementById("ajouter-depense").addEventListener('click', () => {
        createFormDepense();
    });
    document.getElementById("actualiser").addEventListener('click', () => {
        setDepenses();

    })
});

async function setPersonneInfo(){
    let dataPersonne = await fetch("http://localhost:3000/personnes/1");
    finalDataPersonne = await dataPersonne.json();
    
    $('.username').empty();
    $('.username').text(finalDataPersonne['nom']);
    $('.montant-act').empty();
    $('.montant-act').text(finalDataPersonne['montantTotal']);

}


async function setDepenses(){
    setPersonneInfo();
    let data = await fetch("http://localhost:3000/depenses");
    finalData = await data.json();


    $('#droit').empty();
    $('#droit').append($('<ul class="cartes" id="cartes">'));
    var count = Object.keys(finalData).length;
    for(let i = 0; i<count; i++){
        $('#droit ul').append($('<li class="carte" id="'+finalData[i]['id']+'">'));
        $('#'+ finalData[i]['id'] +'').append($('<p class="carte-element titre-depense">').text(finalData[i]['titre']));
        $('#'+ finalData[i]['id'] +'').append($('<p class="carte-element body-depense">').text(finalData[i]['corps']));
        $('#'+ finalData[i]['id'] +'').append($('<p class="carte-element montant">').text(finalData[i]['montant']));
        //$('#'+ finalData[i]['id'] +'').append($('<p">').text("this is a button linked to "+finalData[i]['id']+""));
        $('#'+ finalData[i]['id'] +'').append($('<li class="elemen-cache">').append($('<button type="submit" class="btn-cache" onClick="FillFormDepense('+finalData[i]['id']+')">').text('Modifier')));
        $('#'+ finalData[i]['id'] +'').append($('<li class="elemen-cache">').append($('<button type="submit" class="btn-cache" onClick="deleteDepense('+finalData[i]['id']+')">').text('Supprimer')));
        
    }
   }

async function modifierDepense(depenseId, newJson){
    let data = await fetch("http://localhost:3000/depenses/"+depenseId);
    finalData = await data.json();
    fetch("http://localhost:3000/depenses/"+depenseId,{
        method: 'DELETE'
    });
    saveJSON(depenseId, newJson)
}


async function saveJSON(id, json){
    let data = await fetch("http://localhost:3000/depenses/"+id);
    finalData = await data.json();

    fetch("http://localhost:3000/depenses/"+id,{
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(json)
    });
    setDepenses();

}

async function deleteDepense(depensesId){
    fetch("http://localhost:3000/depenses/"+depensesId,{
        method: 'DELETE'
    });
    setDepenses();

}

async function augmenterSolde(){
    let data = await fetch("http://localhost:3000/personnes/1");
    finalData = await data.json();
    let number = parseInt(finalData['montantTotal']);
    number = number + parseInt(document.getElementById('aug-plafond').value);
    finalData['montantTotal'] = number;
    fetch("http://localhost:3000/personnes/1", {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(finalData)
    });
    let form_aug = document.getElementById("form-aug");
    form_aug.style.display = "none";
    setPersonneInfo();
}


async function setSolde(newSoldes){
    
    alert(document.getElementById('montant-act').value)

    fetch("http://localhost:3000/personnes/1", {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "montantTotal": newSoldes
        })
    });
    
}

function createFormDepense(){
    $("#droit").empty();
    $("#droit").append('<form class="form-add-modifier-dep" method="POST"">')
    $(".form-add-modifier-dep").append($('<label for="titre-dep" class="label-titre-dep">').text('Le titre de la dépense'))
        .append($('<input type="text" id="titre-dep" class="titre-dep">'))
        .append($('<label for="body-dep" class="label-body-dep">').text('Le corps de la dépense'))
        .append($('<input type="text" id="body-dep" class="body-dep">'))
        .append($('<label for="montant-dep" class="label-montant-dep">').text('Le montant de la dépense'))
        .append($('<input type="number" id="montant-dep" class="montant-dep">'))
        .append($('<button type="submit" value="Valider" class="valider" id="validerToAdd">').text('Valider'))
        document.getElementById("validerToAdd").addEventListener('click', async function (){
            setSolde(parseInt(document.getElementById('montant-dep').value) - 10000)
            fetch("http://localhost:3000/depenses", {
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "titre": document.getElementById('titre-dep').value,
                "corps": document.getElementById('body-dep').value,
                "montant": parseInt(document.getElementById('montant-dep').value)
              })
            });
        });
    }

async function FillFormDepense(depenseId){
    let data = await fetch("http://localhost:3000/depenses/"+depenseId);
    finalData = await data.json();

    $("#droit").empty();
    $("#droit").append('<form class="form-add-modifier-dep">')
    $(".form-add-modifier-dep").append($('<label for="titre-dep" class="label-titre-dep">').text('Le titre de la dépense'))
        .append($('<input type="text" id="titre-dep" class="titre-dep">').val(finalData['titre']))
        .append($('<label for="body-dep" class="label-body-dep">').text('Le corps de la dépense'))
        .append($('<input type="text" id="body-dep" class="body-dep">').val(finalData['corps']))
        .append($('<label for="montant-dep" class="label-montant-dep">').text('Le montant de la dépense'))
        .append($('<input type="number" id="montant-dep" class="montant-dep">').val(finalData['montant']))
        .append($('<a class="valider" id="valider">').text('Valider'))
    document.getElementById("valider").addEventListener('click', () => {
        saveJSON(depenseId, {
            "titre": document.getElementById('titre-dep').value,
            "corps": document.getElementById('body-dep').value,
            "montant": document.getElementById('montant-dep').value,
            "uri": "http://localhost:3000/depenses/"+depenseId
          })
    });
}


